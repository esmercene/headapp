import React, { useState, useEffect, useContext } from 'react';
import AdminView from '../components/AdminView';
import CustomerView from '../components/CustomerView';
import { Row } from 'react-bootstrap';


import UserContext from '../UserContext';

export default function Products(){
	const [ products, setProducts ] = useState([])

	const { user } = useContext(UserContext)

	const fetchData = () => {
		fetch(`${ process.env.REACT_APP_API_URL}/products/`)
		.then(response => response.json())
		.then(data => {
			setProducts(data)
		})
	}

	useEffect(() => {
		fetchData()
	}, [])


	return(
		<Row className='mx-auto'>
			{
				user.isAdmin === true
				?
				<AdminView productsData={products} fetchData={fetchData}/>
				:
				<CustomerView productsData={products}/>
			}
	 </Row>
	)
}