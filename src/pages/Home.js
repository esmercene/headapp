import React from 'react'
// import Banner from '../components/Banner'
// import Highlights from '../components/Highlights'
import { Container } from 'react-bootstrap'
import Carousel from '../components/Carousel'
import Highlights from '../components/Highlights'
import Footer from '../components/Footer'
import Review from '../components/Review'

export default function Home(){
	

	return(
		<>
            <Carousel/>
            <Highlights/>
			{/* <Review/> */}
			<Footer/>
			{/* <Banner data={pageData}/>
			<Container fluid>
				<h2 className="text-center mb-4">Featured Products</h2>
				<Highlights/>
			</Container> */}
		</>
	)
}