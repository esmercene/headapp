import { useState, useEffect, useContext } from 'react';
import { useParams, useNavigate, Link } from 'react-router-dom';
import { Card, Container, Button, InputGroup, FormControl, Row, Col, Carousel } from 'react-bootstrap';

import Swal from 'sweetalert2'
import UserContext from '../UserContext';
import background from '../media/background1.jpg'
import hp1 from '../media/hp-001.png'
import hp2 from '../media/hp-002.png'
import hp3 from '../media/hp-003.png'
import hp4 from '../media/hp-004.png'
import hp5 from '../media/hp-005.png'

import '../App.css'


export default function Specific(){
	const { user } = useContext(UserContext)
    const navigate = useNavigate()
	const [id, setId] = useState("")
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [qty, setQty] = useState(1)
	const [price, setPrice] = useState(0)
	

	const { productId } = useParams();

		const create = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/create`,{
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({

				productId: productId,
				userId: user.id
		    })
		}).then(response => response.json())
		  .then(result => {
		  	if (result) {
		  		Swal.fire({
		  			title: "SUCCESS!",
		  			icon: "success",
		  			text: "You have ordered successfully! "
		  		})

		  		navigate('/products')

			}
		  	 else {
		  		Swal.fire({
		  			title: "Sorry!",
		  			icon: "error",
		  			text: "Admin Cannot Place An Order!"
		  		})
		  	}
		  })
	}
	useEffect(() => {
		fetch(`${ process.env.REACT_APP_API_URL}/products/${ productId }`)
		.then(response => response.json())
		.then(result => {
			setId(result._id)
			setName(result.name)
			setDescription(result.description)
			setPrice(result.price)
		})
	
	}, [productId])

	// const reduceQty = () => {
	// 	if(qty <= 1){
	// 		Swal.fire({
    //             title: "Error!",
    //             icon: "error",
    //             text: "Quantity can't be lower than 1."
    //         })
            
	// 	}else{
	// 		setQty(qty - 1)
	// 	}
	// }

	// const addToCart = () => {
	// 	let alreadyInCart = false
	// 	let productIndex
	// 	let message
	// 	let cart = []

	// 	if(localStorage.getItem('cart')){
	// 		cart = JSON.parse(localStorage.getItem('cart'))
	// 	}

	// 	for(let i = 0; i < cart.length; i++){
	// 		if(cart[i].productId === id){
	// 			alreadyInCart = true
	// 			productIndex = i
	// 		}
	// 	}

	// 	if(alreadyInCart){
	// 		cart[productIndex].quantity += qty
	// 		cart[productIndex].subtotal = cart[productIndex].price * cart[productIndex].quantity
	// 	}else{
	// 		cart.push({
	// 			'productId' : id,
	// 			'name': name,
	// 			'price': price,
	// 			'quantity': qty,
	// 			'subtotal': price * qty
	// 		})		
	// 	}

	// 	localStorage.setItem('cart', JSON.stringify(cart))

	// 	if(qty === 1){
	// 		message = "1 item added to cart."
	// 	}else{
	// 		message = `${qty} items added to cart.`
	// 	}

	// 	alert(message)
	// }

	// const qtyInput = (value) => {
	// 	if(value === ''){
	// 		value = 1
	// 	}else if(value === "0"){
    //         Swal.fire({
    //             title: "Error!",
    //             icon: "error",
    //             text: "Quantity can't be lower than 1."
    //         })
	// 		value = 1
	// 	}

	// 	setQty(value)
	// }
	


	return (
		// <Container>
		// 	<Card className="mt-5">
		// 		<Card.Header className="bg-primary text-white text-center pb-0"><h4>{name}</h4></Card.Header>
		// 		<Card.Body>
		// 			<Card.Text>{description}</Card.Text>
		// 			<h6>Price: <span className="text-warning">₱{price}</span></h6>
		// 			<h6>Quantity:</h6>
		// 			<InputGroup className="qty mt-2 mb-1">
		// 				<InputGroup.Prepend className="d-none d-md-flex">
		// 					<Button variant="secondary" onClick={reduceQty}>-</Button>
		// 				</InputGroup.Prepend>
		// 				<FormControl type="number" min="1" value={qty} onChange={e => qtyInput(e.target.value)}/>
		// 				<InputGroup.Append className="d-none d-md-flex">
		// 					<Button variant="secondary" onClick={() => setQty(qty + 1)}>+</Button>
		// 				</InputGroup.Append>
		// 			</InputGroup>
		// 		</Card.Body>
		// 		<Card.Footer>
		// 		{user.id !== null
		// 			? user.isAdmin === true
		// 				? <Button variant="danger" block disabled>Admin can't Add to Cart</Button>
		// 				: <Button variant="primary" block onClick={addToCart}>Add to Cart</Button>
		// 			: <Link className="btn btn-warning btn-block" to={{pathname: '/login', state: { from: 'cart'}}}>Please Log in to Add to Cart</Link>
		// 		}
	    //   		</Card.Footer>
		// 	</Card>
		// </Container>
		<div className='specificbg' style={{backgroundImage: `url(${background}` }} >
		<Container>	
		<Button className="mt-5 btn-secondary-custom gobackbtn" variant="secondary" onClick={() => navigate(-1)}>Go back</Button>
				<Row>
				<Col md={6}>
                    <Carousel fade className='carouselspec'>
                        <Carousel.Item>
                            <img
                            className="d-block w-100"
                            src={hp1}
                            alt="First slide"
                            />
                        </Carousel.Item>
						<Carousel.Item>
                            <img
                            className="d-block w-100"
                            src={hp2}
                            alt="First slide"
                            />
                        </Carousel.Item>
						<Carousel.Item>
                            <img
                            className="d-block w-100"
                            src={hp3}
                            alt="First slide"
                            />
                        </Carousel.Item>
						<Carousel.Item>
                            <img
                            className="d-block w-100"
                            src={hp4}
                            alt="First slide"
                            />
                        </Carousel.Item>
						<Carousel.Item>
                            <img
                            className="d-block w-100"
                            src={hp5}
                            alt="First slide"
                            />
                        </Carousel.Item>
                    </Carousel>
                </Col>
					<Col  classsName=''>
							<Card className='cardspecific mx-auto mt-0'>
								<Card.Body className="ordercard  text-primary">
									<Card.Title  className='mt-3'>{name}</Card.Title>
									<Card.Subtitle>Description:</Card.Subtitle>
									<Card.Text className="text-light">{description}</Card.Text>
									<Card.Subtitle className="mt-5">Price:</Card.Subtitle>
								<Card.Text className="text-warning">PhP {price}</Card.Text>							{   user.id !== null ?
											<Button variant="primary" onClick={() => create(
											productId)} >Order</Button>
										:
											<Link className="mt-3 mb-5 btn btn-danger btn-block" to="/login">Log in to Order </Link>

									}
									
								</Card.Body>		
							</Card>
						</Col>
					
					
					
			</Row>		
		</Container>
		</div>
	)
}
	
