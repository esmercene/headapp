import React, { useState, useEffect } from 'react';
import { Row, Container, Card, Accordion, Jumbotron } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import moment from 'moment';
import background from '../media/background1.jpg'

export default function Orders(){

	const [ordersList, setOrdersList] = useState([])
	const [orders, setOrders] = useState([])


	useEffect(()=> {

		fetch(`${ process.env.REACT_APP_API_URL}/orders`, {
			// headers: {
			// 	Authorization: `Bearer ${ localStorage.getItem('token') }`
			// }
		})
		.then(response => response.json())
		.then(data => {
			if(data.length > 0){
				let orders = data.map((item, index)=> {
					return(
						<Card key={item._id}>
							<Accordion.Toggle as={Card.Header} eventKey={index + 1} className="bg-secondary text-white">
								Order #{index + 1} - Purchased on: {moment(item.purchasedOn).format("MM-DD-YYYY")} (Click for Details)
							</Accordion.Toggle>
							<Accordion.Collapse eventKey={index + 1}>
								<Card.Body>
									<h6>Items:</h6>
									<ul>
									{
										item.products.map((subitem) => {
											return (
												<li key={subitem.productId}>{subitem.productName} - Quantity: {subitem.quantity}</li>
											)
										})
									}
									</ul>
									<h6>Total: <span className="text-warning">₱{item.totalAmount}</span></h6>
								</Card.Body>
							</Accordion.Collapse>
						</Card>
					)
				})
	
				setOrdersList(orders)				
			}
		})
	}, [])

	return(
		<>
		<Row className='orderbg'style={{backgroundImage: `url(${background}` }}>
			
			?<Accordion>
					<h3 className="text-center text-light">You Have No Order Yet!</h3>
					<h4 className="text-center"><Link to="/products">Shop Now.</Link></h4>
			</Accordion>
			</Row>	
			:
			<Row>
				<h2 className="text-center text-light my-4">Order History</h2>
				<Accordion>
					{ordersList}
				</Accordion>
			</Row>
		
		</>
	)
}