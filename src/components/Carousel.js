import React from 'react';
import  Carousel  from 'react-bootstrap/Carousel';
import background1  from '../media/bg1.jpg'
import background2  from '../media/bg2.jpg'
import background3  from '../media/bg3.jpg'


export default function FeaturedProducts() {
  return (
    <Carousel justify-content-between fade>
      <Carousel.Item>
        <img
          className="w-100 "
          src={background1}
          alt="Specials"
        />
        <Carousel.Caption>
          <h3>Specials</h3>
          <p>Get the Hottest Deals with Our Specials</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="w-100"
          src={background2}
          alt="Collections"
        />

        <Carousel.Caption>
          <h3>Collections</h3>
          <p>Choose from our latest collections</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="w-100"
          src={background3}
          alt=""
        />

        <Carousel.Caption>
          <h3>Popular</h3>
          <p>
            Grab great deals in our popular products.
          </p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
}

