import React, { useEffect, useState } from 'react';
import Product from '../components/Product';
import { Row,Col } from 'react-bootstrap';
import background from '../media/background1.jpg'
import '../App.css'

export default function CustomerView({productsData}){

	const [products, setProducts] = useState([])

	useEffect(() => {

		const productsArr = productsData.map(productData => {
			if(productData.isActive === true){
				return <Product data={productData} key={productData._id} breakPoint={4}/>
			}else{
				return null
			}
		})

		setProducts(productsArr)
	}, [productsData])

	return(
		<>
			
			<Row className='productsbg' style={{backgroundImage: `url(${background}` }}>
			<h2 className="text-center text-light my-4">All Products</h2>
			{products}
			
				
			</Row>
			
		</>
	)
}