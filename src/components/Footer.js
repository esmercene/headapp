import React from 'react';
import {
  MDBFooter,
  MDBContainer,
  MDBIcon,
  MDBInput,
  MDBCol,
  MDBRow,
  MDBBtn
} from 'mdb-react-ui-kit';
import * as Icon from 'react-bootstrap-icons';

export default function App() {
  return (
    
    <MDBFooter className='text-center' color='white' bgColor='dark'>
        
      <MDBContainer className='p-4'>

      <section className='mb-2'>
          <p>
            Whether you're new to purchasing a headphone, we are glad you are here. Stay Informed of Our Special Offers.
          </p>
        </section>
        <section className=''>
          <form action=''>
            <MDBRow className='d-flex justify-content-center'>
              <MDBCol size="auto">
                <p className='pt-2'>
                  <strong>Sign up for our newsletter:</strong>
                </p>
              </MDBCol>

              <MDBCol md='5' start>
                <MDBInput contrast type='email' label='Email address' className='mb-3' />
              </MDBCol>

              <MDBCol size="auto">
                <MDBBtn outline color='light' type='submit' className='mb-2'>
                  Subscribe
                </MDBBtn>
              </MDBCol>
            </MDBRow>
          </form>
        </section>

      
            
      
      </MDBContainer>

      <div className='text-center p-3' style={{ backgroundColor: 'rgba(0, 0, 0, 0.3)' }}>
        Terms of Use | Privacy Policy | All Rights Reserved | © 2022 Copyright:<span>  </span>   
        <a className='text-primary' href='https://headpapp.vercel.app/'>
          headP.com
        </a>
      </div>
    </MDBFooter>
  );
}