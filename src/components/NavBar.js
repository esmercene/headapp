import {Navbar, Nav, NavDropdown, Badge } from 'react-bootstrap'
import { Link, NavLink } from 'react-router-dom'
import logo from '../media/logo.png'
import UserContext from '../UserContext'
import {useState, useEffect, useContext} from 'react'



export default function NavBar(){

	// const [ user, setUser] = useState(localStorage.getItem('email'))

	const {user} = useContext(UserContext)


	return (
		<Navbar className='headpnav'sticky="top" expand="md" variant="light">
				<Navbar.Brand as={Link} to="/">
                    <img
                        src={logo}
                        width="150"
                        height="80"
                        className="d-inline-block align-top"
                        alt="headP Logo"
                    />
                </Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav" >
					<Nav className="m-auto" >
						<Nav.Link as={NavLink} to="/">Home</Nav.Link>
						<Nav.Link as={NavLink} to="/products">
						{user.isAdmin === true
						?
						<span>Admin Dashboard</span>
						:
						<span>All Products</span>
					}	
						</Nav.Link>
					</Nav>
						<Nav className="ml-auto">	
						{	(user.id !== null) 
						?
							user.isAdmin === true 
							?
							  <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
							:
							<>
								{/* <Nav.Link as={NavLink} to="/cart">Cart</Nav.Link> */}
								<Nav.Link as={NavLink} to="/orders">Orders</Nav.Link>
								<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                            </>
						:
						<>
						<Nav.Link as={NavLink} to={{pathname: '/login', state: { from: 'navbar'}}} className='me-0'>Log In  <span>|</span>  </Nav.Link>
						<Nav.Link as={NavLink} to="/signup" className='ms-0'>SignUp</Nav.Link>
						</>
                        }
					</Nav>
				</Navbar.Collapse>
			</Navbar>
	)
}
