import { Row, Col, Card, CardImage, Tooltip, OverlayTrigger} from 'react-bootstrap'
import React from 'react'
import { Link } from 'react-router-dom'
import banner1  from '../media/banner1.jpg' 
import banner2  from '../media/banner2.jpg' 
import banner3  from '../media/banner3.jpg' 


export default function Highlights(){
	return (
		<Row className="mt-4 mb-3">
            <h1 className="text-center mt-5 mb-2">Our Featured Products</h1>
            <h5 className="text-center mt-2 mb-5">"We're Like APPLE, We Polish other People's Idea"</h5>
	            <Col xs={12} md={4} className='justify-content-around'  >
	                <Card className=" mb-5 mb-lg-0 ms-3">
					                    <Card.Body>
					                        <Card.Img
									          src={require('../media/banner1.jpg')}
									        />
					                        <Card.Title>
					                            <h2 className='text-center mt-3'>Specials</h2>
					                        </Card.Title>
					                        <Card.Text className='text-center'>
					                           Shop and discover our special products with different options you can choose from.
					                        </Card.Text>
                                            <Link className="btn btn-primary btn-block shpnwbtn" to={`/products/`}>Shop Now</Link>
					                    </Card.Body>
	                </Card>
	            </Col>
	            <Col xs={12} md={4} className='justify-content-around' >
					                <Card className=" mb-5 mb-lg-0 mx-auto">
					                    
					                    <Card.Body>
                                        <Card.Img
									          src={require('../media/banner2.jpg')}
									        />
					                        <Card.Title>
					                            <h2 className='text-center mt-3'>Collections</h2>
					                        </Card.Title>
					                        <Card.Text className='text-center'>
					                            Choose from our collections and great deals you can have from our various and limited products you can avail.
					                        </Card.Text>
                                            <Link className="btn btn-primary btn-block shpnwbtn" to={`/products/`}>Shop Now</Link>
					                    </Card.Body>
					                </Card>
	            </Col>
	            <Col xs={12} md={4} className='justify-content-around' >
					                <Card className="mb-5 mb-lg-2 me-3">
					                    <Card.Body>
                                        <Card.Img
									          src={require('../media/banner3.jpg')}
									        />
					                        <Card.Title>
					                            <h2 className='text-center mt-3'>Popular Items</h2>
					                        </Card.Title>
					                        <Card.Text className='text-center'>
					                            Have a look in our latest products and new designs in our popular items and get a great deals when you shop.
					                        </Card.Text>
                                            <Link className="btn btn-primary btn-block shpnwbtn" to={`/products/`}>Shop Now</Link>
					                    </Card.Body>
					                </Card>
	            </Col>
	    </Row>
	)
}