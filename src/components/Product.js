import React from 'react'
import { Col, Card, Row, Carousel } from 'react-bootstrap'
import { useParams, Link } from 'react-router-dom'
import background from '../media/background1.jpg'
import '../App.css'
import hp1 from '../media/hp-001.png'
import hp2 from '../media/hp-002.png'
import hp3 from '../media/hp-003.png'
import hp4 from '../media/hp-004.png'
import hp5 from '../media/hp-005.png'
import hp6 from '../media/hp-006.png'
import hp7 from '../media/hp-007.png'


export default function Product(props){
	const { breakPoint, data } = props
    const { productId } = useParams();
	const { _id, name, description, price } = data

	return(
		
			<Col xs={12} md={breakPoint} className="productcard mb-4" >
				<Card className="card1">
					<Card.Body className="productcardbody">
						<Card.Title className="text-center text-dark card2">
							<Link to={`/products/${_id}`}>{name}</Link>
						</Card.Title>
						<Carousel>
						<Carousel>
                        <Carousel.Item>
                            <img
                            className="d-block "
                            src={hp1}
                            />
                        </Carousel.Item>
						<Carousel.Item>
                            <img
                            className="d-block"
                            src={hp2}
                            
                            />
                        </Carousel.Item>
						<Carousel.Item>
                            <img
                            className="d-block"
                            src={hp3}
                            />
                        </Carousel.Item>
						<Carousel.Item>
                            <img
                            className="d-block"
                            src={hp4}
                            />
                        </Carousel.Item>
						<Carousel.Item>
                            <img
                            className="d-block"
                            src={hp5}
                            />
                        </Carousel.Item>
						<Carousel.Item>
                            <img
                            className="d-block"
                            src={hp6}
                            />
                        </Carousel.Item>
						<Carousel.Item>
                            <img
                            className="d-block"
                            src={hp7}
                            />
                        </Carousel.Item>
                    </Carousel>
						</Carousel>
						<Card.Text className="card3 text-center text-light">{description}</Card.Text>
						<h5 className="priceprdct text-center">₱ {price}</h5>
					</Card.Body>
					<Card.Footer className="footercard">
						<Link className="btn btn-primary detailsbtn" to={`/products/${_id}`}>Details</Link>
					</Card.Footer>
				</Card>
			</Col>
		
	)
}