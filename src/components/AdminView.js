import React, { useEffect, useState, useContext } from 'react'
import { Form, Table, Button, Modal, Accordion, Card, Row } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import moment from 'moment';
import Swal from 'sweetalert2'
import UserContext from '../UserContext'
import background from '../media/background2.jpg'
import '../App.css'

export default function AdminView(props){
    const {user, setUser, unsetUser} = useContext(UserContext)
	const { productsData, fetchData } = props
	const [ products, setProducts ] = useState([])
	const [id, setId] = useState("")
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)
	const [showAdd, setShowAdd] = useState(false)
	const [showEdit, setShowEdit] = useState(false)
	const [toggle, setToggle] = useState(false)
	const [ordersList, setOrdersList] = useState([])

	const openAdd = () => setShowAdd(true)
	const closeAdd = () => setShowAdd(false)

	const openEdit = (productId) => {

		setId(productId)

		fetch(`${ process.env.REACT_APP_API_URL }/products/${ productId }`)
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})

		setShowEdit(true)
	}

	const closeEdit = () => {
		setName("")
		setDescription("")
		setPrice(0)
		setShowEdit(false)
	}

	const addProduct = (event) => {

		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(response => response.json())
		.then(data => {
			if(data){
				fetchData()
				Swal.fire({
                    title: "SUCCESS!",
                    icon: "success",
                    text: "Product Created Successfully! "
                })
				setName("")
				setDescription("")
				setPrice(0)
				closeAdd()
			}else{
				Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: "Something Went Wrong! "
                })
				closeAdd()
			}
		})
	}

	const editProduct = (event, productId) => {

		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/${ productId }/update`, {
		method: 'PATCH',
		headers: {
			// Authorization: `Bearer ${ localStorage.getItem('token') }`,
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			name: name,
			description: description,
			price: price
		})
	})
	.then(response => response.json())
	.then(data => {
		if(data){
			fetchData()
			Swal.fire({
                title: "SUCCESS!",
                icon: "success",
                text: "Product Updated Successfully! "
            })
			setName("")
			setDescription("")
			setPrice(0)
			closeEdit()
		}else{
			Swal.fire({
                title: "Error!",
                icon: "error",
                text: "Something Went Wrong! "
            })
			closeEdit()
		}
	})
	}

	const archiveProduct = (productId) => {
			fetch(`${process.env.REACT_APP_API_URL}/products/${ productId }/archive`, {
			method: 'PATCH',
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`,
			}
		})
		.then(response => response.json())
		.then(result => {
			if(result){
				fetchData()
				Swal.fire({
                    title: "SUCCESS!",
                    icon: "success",
                    text: "Product Archived Successfully! "
                })
			}else{
				Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: "Something Went Wrong! "
                })
			}
		})
	}

	const activateProduct = (productId) => {
			fetch(`${process.env.REACT_APP_API_URL}/products/${ productId }/activate`, {
			method: 'PATCH',
			// headers: {
			// 	Authorization: `Bearer ${ localStorage.getItem('token') }`,
			// }
		})
		.then(response => response.json())
		.then(data => {
			if(data){
				fetchData()
				Swal.fire({
                    title: "SUCCESS!",
                    icon: "success",
                    text: "Product Activated Successfully! "
                })
			}else{
				Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: "Something Went Wrong! "
                })
			}
		})
	}

	useEffect(()=> {
		fetch(`${process.env.REACT_APP_API_URL}/orders/create`, {
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`,
			}
		})
		.then(response => response.json())
		.then(data => {
			let orders = data.map((user, index)=> {
				return(
					<Card key={user.userId}>
						<Accordion.Toggle as={Card.Header} eventKey={index + 1} className="bg-secondary text-white">
							Orders for user <span className="text-warning">{user.email}</span>
						</Accordion.Toggle>
						<Accordion.Collapse eventKey={index + 1}>
							<Card.Body>
								{user.orders.length > 0
									?
									user.orders.map((order) => {
										return (
											<div key={order._id}>
												<h6>Purchased on {moment(order.purchasedOn).format("MM-DD-YYYY")}:</h6>
												<ul>
												{
													order.products.map((product)=> {
														return (
															<li key={product._id}>{product.productName} - Quantity: {product.quantity}</li>
														)															
													})
												}
												</ul>
												<h6>Total: <span className="text-warning">₱{order.totalAmount}</span></h6>
												<hr/>
											</div>
										)
									})
									:
									<span>No orders for this user yet.</span>
								}
							</Card.Body>
						</Accordion.Collapse>
					</Card>
				)
			})
			setOrdersList(orders)
		})
	}, [])

	const toggler = () => {
		if(toggle === true){
			setToggle(false)
		}else{
			setToggle(true)
		}
	}

	useEffect(() => {

		const productsArr = productsData.map(productData => {
			return (
				<tr key={productData._id}>
					<td className='text-primary'>{productData.name}</td>
					<td>{productData.description}</td>
					<td className='text-warning'>PhP {productData.price}</td>
					<td>
						{productData.isActive
							?
							<span>Available</span>
							:
							<span className='text-danger'>Unavailable</span>
						}
					</td>
					<td>
						<Button className="updatebtn me-1 mb-1" variant="primary" size="sm" onClick={() => openEdit(productData._id)}>Update</Button>
						{productData.isActive
							?
							<Button className="disablebtn me-1 mb-1" variant="danger" size="sm" onClick={() => archiveProduct(productData._id)}>Disable</Button>
							:
							 <Button className="disablebtn" variant="success" size="sm" onClick={() => activateProduct(productData._id)}>Enable</Button>
						}
					</td>
				</tr>
				)
			})
			
		setProducts(productsArr)	
	}, [productsData])

	return(
        
		<React.Fragment >
            <Row style={{backgroundImage: `url(${background}` }}>
			<div className="text-center my-4">
				<h2 className='text-white'>Admin Dashboard</h2>
				<div className="d-flex justify-content-center">
					<Button className="addproductbtn me-2"  onClick={openAdd}>Add New Product</Button>
					{toggle === false
					? <Button className='showusersbtn'  onClick={()=> toggler()}>Show Users</Button>
					: <Button className='showproductbtn'  onClick={()=> toggler()}>Show Product Details</Button>
					}
					
				</div>
			</div>
			{toggle === false
			?
			<Table >
				<thead className="bg-secondary text-white">
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody className='text-white'>
					{products}
				</tbody>						
			</Table>
			:
			// <Accordion>
			// 	{ordersList}
			// </Accordion>
            <Table striped bordered hover responsive>
				<thead className="bg-secondary text-white">
					<tr>
						<th>Name</th>
						<th>Email</th>
						<th>Number</th>
						<th>isAdmin</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{id}
				</tbody>						
			</Table>
			}
			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add New Product</Modal.Title>
					</Modal.Header>
					<Modal.Body>
							<Form.Group controlId="productName">
								<Form.Label>Name:</Form.Label>
								<Form.Control type="text" placeholder="Enter product name" value={name} onChange={e => setName(e.target.value)} required/>
							</Form.Group>

							<Form.Group controlId="productDescription">
								<Form.Label>Description:</Form.Label>
								<Form.Control type="text" placeholder="Enter product description" value={description} onChange={e => setDescription(e.target.value)} required/>
							</Form.Group>

							<Form.Group controlId="productPrice">
								<Form.Label>Price:</Form.Label>
								<Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required/>
							</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>	
			</Modal>

			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editProduct(e, id)}>
					<Modal.Header closeButton>
						<Modal.Title>Edit Product</Modal.Title>
					</Modal.Header>
					<Modal.Body>
							<Form.Group controlId="productName">
								<Form.Label>Name:</Form.Label>
								<Form.Control type="text" placeholder="Enter product name" value={name} onChange={e => setName(e.target.value)} required/>
							</Form.Group>

							<Form.Group controlId="productDescription">
								<Form.Label>Description:</Form.Label>
								<Form.Control type="text" placeholder="Enter product description" value={description} onChange={e => setDescription(e.target.value)} required/>
							</Form.Group>

							<Form.Group controlId="productPrice">
								<Form.Label>Price:</Form.Label>
								<Form.Control type="number" placeholder="Enter product price" value={price} onChange={e => setPrice(e.target.value)} required/>
							</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>	
			</Modal>
        </Row>
		</React.Fragment>

	)
}