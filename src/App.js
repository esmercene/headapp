import './App.css';
import React, { useState, useEffect } from "react"
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
// import 'bootswatch/dist/cosmo/bootstrap.min.css';
import NavBar from './components/NavBar'
import CustomerView from './components/CustomerView'
import Home from './pages/Home'
import SignUp from './pages/SignUp'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Products from './pages/Products'
import Specific from './pages/Specific'
import MyCart from './pages/MyCart'
import Orders from './pages/Orders'
// import Error from './pages/Error';
import { UserProvider } from './UserContext';
import { Container } from 'react-bootstrap'

export default function App() {
	const [user, setUser, unsetUser] = useState({
		id: null,
		isAdmin: null
	})

	useEffect(() => {
		fetch(`${ process.env.REACT_APP_API_URL }/users/details`, {
			headers: { Authorization: `Bearer ${ localStorage.getItem('token') }`}
		})
		.then(response => response.json())
		.then(data => {

			if(typeof data._id !== 'undefined'){
				setUser({ id: data._id, isAdmin: data.isAdmin })
			}else{
				setUser({ id: null, isAdmin: null })
			}
		})

	}, [])

  const [cart, setCart] = useState([])
  return (
    <>
  	<UserProvider value={{user, setUser, unsetUser}}>
	  	<Router>
		    <NavBar/>
		   
       
		    <Routes>
				<Route  path="/" element={<Home/>}/>
				<Route  path="/signup" element={<SignUp/>}/>
				<Route  path="/login" element={<Login/>}/>
				<Route path="/products" element={<Products/>}/>
				<Route path="/products/:productId" element={<Specific/>}/>
				<Route path="/cart" element={<MyCart/>}/>
				<Route path="/orders" element={<Orders/>}/>
				<Route path="/logout" element={<Logout/>}/>
				{/* <Route element={Error}/> */}
		    </Routes>
     
	    </Router>
	</UserProvider>
  </>
  );
}